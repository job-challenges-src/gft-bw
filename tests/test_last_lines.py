from src.last_lines import last_lines


def test_last_lines():
    lines = last_lines('my_file.txt')
    assert next(lines) == 'This is line 50000'
