import csv
from pathlib import Path

from src.reconcile_accounts import reconcile_accounts


def test_reconcile_accounts():
    expected_output1 = [
        ['2020-12-04', 'Tecnologia', '16.00', 'Bitbucket', 'FOUND'],
        ['2020-12-04', 'Jurídico', '60.00', 'LinkSquares', 'FOUND'],
        ['2020-12-05', 'Tecnologia', '50.00', 'AWS', 'MISSING']
    ]

    expected_output2 = [
        ['2020-12-04', 'Tecnologia', '16.00', 'Bitbucket', 'FOUND'],
        ['2020-12-04', 'Jurídico', '60.00', 'LinkSquares', 'FOUND'],
        ['2020-12-05', 'Tecnologia', '49.99', 'AWS', 'MISSING'],
    ]
    transactions1 = list(csv.reader(Path('transactions1.csv').open()))
    transactions2 = list(csv.reader(Path('transactions2.csv').open()))
    out1, out2 = reconcile_accounts(transactions1, transactions2)
    assert expected_output1 == out1
    assert expected_output2 == out2