from datetime import datetime, timedelta
from typing import Dict, List, Tuple


def _find_transaction(current_key: str, keys: List[str]) -> Tuple[bool, str]:
    date_format = '%Y-%m-%d'
    date = current_key.split('|')[0]
    yesterday = (datetime.strptime(date, date_format) - timedelta(days=1)).strftime(date_format)
    yesterday_key = current_key.replace(date, yesterday)
    if yesterday_key in keys:
        return True, yesterday_key
    
    if current_key in keys:
        return True, current_key
    
    tomorrow = (datetime.strptime(date, date_format) + timedelta(days=1)).strftime(date_format)
    tomorrow_key = current_key.replace(date, tomorrow)
    if tomorrow_key in keys:
        return True, tomorrow_key
    return False, ''


def reconcile_accounts(transaction1: List[List[str]], transaction2: List[List[str]]) -> Tuple[List[List[str]], List[List[str]]]:
    """Fiz o suficiente pra deixar funcional mas pra ter certeza que todas as regras foram atendidas precisaria de dados para teste."""
    transaction_data1: Dict[str, List[str]] = {'|'.join(i): i for i in sorted(transaction1, key=lambda x: x[0])}
    transaction_data2: Dict[str, List[str]] = {'|'.join(i): i for i in sorted(transaction2, key=lambda x: x[0])}
    transaction_data2_keys = list(transaction_data2.keys())

    for key in transaction_data1.keys():
        is_finded, key2 = _find_transaction(key, transaction_data2_keys)
        if not is_finded:
            transaction_data1[key].append('MISSING')
            continue
        
        transaction_data1[key].append('FOUND')
        transaction_data2[key2].append('FOUND')
        transaction_data2_keys.remove(key2)
    
    for key in transaction_data2_keys:
        transaction_data2[key].append('MISSING')
            
    return list(transaction_data1.values()), list(transaction_data2.values())

