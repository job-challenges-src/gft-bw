from pathlib import Path
from typing import Iterator


def last_lines(file_name: str = 'my_file.txt', **kwargs) -> Iterator:
    """Foi implementado somente o mínimo necessário pra deixar funcional"""
    file = Path(file_name).open()
    return reversed([i.strip() for i in file.readlines()])
